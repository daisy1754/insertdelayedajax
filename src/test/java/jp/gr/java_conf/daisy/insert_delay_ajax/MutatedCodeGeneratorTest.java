package jp.gr.java_conf.daisy.insert_delay_ajax;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.io.Resources;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MutatedCodeGeneratorTest {
    @Test
    public void testInsertDelayedObjectIntoOnelinerJs() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        System.out.println(Resources.toString(this.getClass().getResource("/oneliner.js"), Charset.defaultCharset()));
        System.out.println(this.getClass().getResource("/oneliner.js").toURI());
        String mutatedCode = generator.insertDelayedRequestObject(
                localJsResource("/oneliner.js"), 0, delay);
        assertEquals(
                "new DelayedRequest(1000).prototypeJs_Ajax_Request(\"target.php"
                 + "\", {method: \"get\", onComplete: function() {alert('success!')}});",
                getLastLine(mutatedCode)
        );
        MutatedCodeGenerator.cleanupOurAnnotationFromJs(this.getClass().getResource("/oneliner.js").getPath());
    }

    @Test
    public void testInsertDelayedObjectIntoEcJs() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        String mutatedCode = generator.insertDelayedRequestObject(
                localJsResource("/ec.js"), 1065, delay);
        assertEquals(
                Resources.toString(this.getClass().getResource("/ec_test_out.data"), Charset.defaultCharset()),
                extractNonLibaryCode(mutatedCode));
        MutatedCodeGenerator.cleanupOurAnnotationFromJs(this.getClass().getResource("/ec.js").getPath());
    }

    @Test
    public void testInsertDelayedObjectInto2020m() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        String mutatedCode = generator.insertDelayedRequestObject(
                localJsResource("/login_controller.js"), 899, delay);
        assertEquals(
                Resources.toString(this.getClass().getResource("/login_controller.out"), Charset.defaultCharset()),
                extractNonLibaryCode(mutatedCode));
        MutatedCodeGenerator.cleanupOurAnnotationFromJs(this.getClass().getResource("/login_controller.js").getPath());
    }

    @Test
    public void testInsertDelayedObjectIntoEcHtml() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        String mutatedCode = generator.insertDelayedRequestObject(
                localHtmlResource("/ec.html", 2), 1078, delay);
        List<String> mutatedFunctionShouldbe = ImmutableList.of(
                "function reqRunTrans() {",
                "\tnew DelayedRequest(1000).prototypeJs_Ajax_Request(\"runTrans.php\", {",
                "\t\tmethod: \"GET\", parameters: getParams(),",
                "\t\tonSuccess: succeeded",
                "\t});",
                "}");
        extractNonLibaryCode(mutatedCode);
        List<String> mutatedFunction = extractLines(
                mutatedCode,
                mutatedFunctionShouldbe.get(0),
                mutatedFunctionShouldbe.size());
        assertEquals(mutatedFunctionShouldbe, mutatedFunction);
        MutatedCodeGenerator.cleanupOurDataAnnotationFromInlineJs(this.getClass().getResource("/ec.html").getPath());
    }

    @Test
    public void testDelayedScriptLoadForExternalSrc() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        String mutatedHtml = generator.modifyScriptTagForDelayedLoad(
                this.getClass().getResource("/ec.html").getPath(),
                0, delay);

        Document mutatedDoc = Jsoup.parse(mutatedHtml);
        Element insertedScriptTag = mutatedDoc.getElementsByTag("script").get(0);
        assertEquals(Resources.toString(
                this.getClass().getResource("/ec_load_src_with_delay.data"),
                Charset.forName("UTF-8")),
                insertedScriptTag.toString());

        insertedScriptTag.remove();
        Document originalDoc = Jsoup.parse(Resources.toString(
                this.getClass().getResource("/ec.html"),
                Charset.defaultCharset()));
        originalDoc.getElementsByTag("script").first().remove();
        documentsAreEqual(originalDoc, mutatedDoc);
        MutatedCodeGenerator.cleanupOurDataAttrFromHtml(this.getClass().getResource("/ec.html").getPath());
    }

    @Test
    public void testDelayedScriptLoadForLiteralScript() throws Exception {
        MutatedCodeGenerator generator = new MutatedCodeGenerator();
        int delay = 1000;
        String mutatedHtml = generator.modifyScriptTagForDelayedLoad(
                this.getClass().getResource("/ec.html").getPath(),
                2, delay);

        Document mutatedDoc = Jsoup.parse(mutatedHtml);
        Element insertedScriptTag = mutatedDoc.getElementsByTag("script").get(2);
        String insertedScript = insertedScriptTag.data();
        Document originalDoc = Jsoup.parse(Resources.toString(
                this.getClass().getResource("/ec.html"),
                Charset.defaultCharset()));
        Element originalScriptTag
                = Iterables.get(originalDoc.getElementsByTag("script"), 2);
        String originalScript = originalScriptTag.data();
        String lastLine = getLastLine(insertedScript);
        assertTrue(lastLine.endsWith("), 1000);"));
        assertTrue(lastLine.startsWith("setTimeout(loadJsLiteralWithDelay.bind(this, "));
        String insertedScriptBody = lastLine.substring(
                "setTimeout(loadJsLiteralWithDelay.bind(this, '".length(),
                lastLine.length() - "'), 1000);".length());
        assertEquals(originalScript, StringEscapeUtils.unescapeEcmaScript(insertedScriptBody));
        insertedScriptTag.remove();
        documentsAreEqual(originalDoc, mutatedDoc);
        MutatedCodeGenerator.cleanupOurDataAttrFromHtml(this.getClass().getResource("/ec.html").getPath());
    }

    // This method is required because for given doc (typed Document),
    // Jsoup.parse(doc.toString()) is not always equals to doc.
    private void documentsAreEqual(Document doc1, Document doc2) {
        Elements children1 = doc1.children();
        Elements children2 = doc1.children();
        if (!elementsAreEqual(children1, children2)) {
            // This would fail and will print readable result than just calling
            // fail();
            assertEquals(doc1, doc2);
        }
    }

    // This method is required because for given doc (typed Document),
    // Jsoup.parse(doc.toString()) is not always equals to doc.
    private boolean elementsAreEqual(Elements elements1, Elements elements2) {
        if (elements1.size() != elements2.size()) {
            System.err.println("difference in children size " + elements1);
            return false;
        }
        for (int i = 0; i < elements1.size(); i++) {
            Element element1 = elements1.get(i);
            Element element2 = elements2.get(i);
            if (!elementsAreEqual(element1.children(), element2.children())) {
                return false;
            } else if (!element1.toString().equals(element2.toString())) {
                System.err.println("'" + element1.toString()
                        + "' is not equals to " + "'"
                        + element2.toString() + "'");
                return false;
            }
        }
        return true;
    }

    private JavaScriptLocation localJsResource(String resourceLocation) {
        URL inputResourceUrl = this.getClass().getResource(resourceLocation);
        return JavaScriptLocation.jsFile(inputResourceUrl.getFile());
    }

    private JavaScriptLocation localHtmlResource(String resourceLocation, int index) {
        URL inputResourceUrl = this.getClass().getResource(resourceLocation);
        return JavaScriptLocation.inHtmlFile(inputResourceUrl.getFile(), index);
    }

    private String getLastLine(String content) {
        String[] lines = content.split(System.lineSeparator());
        return lines[lines.length - 1];
    }

    private String extractNonLibaryCode(String mutatedCode) {
        return mutatedCode.split(
                MutatedCodeGenerator.LIBRARY_FOOTER + System.lineSeparator())[1];
    }

    private List<String> extractLines(
            String content, String startLine, int numOfLinesToExtract) {
        String[] contents = content.split(System.lineSeparator());
        int index = 0;
        while (!contents[index].equals(startLine)) {
            index++;
        }
        return Arrays.asList(contents)
                .subList(index, index + numOfLinesToExtract);
    }
}
