window.onload = setEventHandler;

function setEventHandler() {
	var slct_type = document.getElementById("reg_type");
	slct_type.onchange = calcPrice;

	var slct_attendee = document.getElementById("reg_attendee");
	slct_attendee.onchange = calcPrice;

	var slct_payment = document.getElementById("reg_payment");
	slct_payment.onchange = calcPrice;

	var inpt_addcart = document.getElementById("addcart");
	inpt_addcart.onclick = addCart;
};

function calcPrice() {
	var price = 0;

	var slct_type = document.getElementById("reg_type");
	var price_type = parseInt(slct_type.value);

	var slct_attendee = document.getElementById("reg_attendee");
	var price_attendee = parseInt(slct_attendee.value);

	var slct_payment = document.getElementById("reg_payment");
	var price_payment = parseInt(slct_payment.value);

	price += price_type + price_attendee + price_payment;
	displayPrice(price);
};

function addCart() {
	disableAddCart();

	if(isValidInput()) {
		reqRunTrans();
	} else {
		alert("Invalid user inputs");
		enableAddCart();
	};
};

function reqRunTrans() {
	new Ajax.Request("runTrans.php", {
		method: "GET", parameters: getParams(),
		onSuccess: succeeded
	});
}

function enableAddCart() {
	var inpt_addcart = document.getElementById("addcart");
	inpt_addcart.disabled = false;
};

function disableAddCart() {
	var inpt_addcart = document.getElementById("addcart");
	inpt_addcart.disabled = true;
};

function succeeded() {
	disableAll();
	jumpToConfirm();
};